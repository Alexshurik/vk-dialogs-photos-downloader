import argparse
import os

import vk

# Последняя версия в момент написания скрипта, используется для всего,
# кроме подгрузки фотографий
VK_API_VERSION = 5.65

# Версия API, используемая для подгрузки фоток
# При использовании версии, выше 5.46, старые фотки не подгружаются
VK_PHOTO_API_VERSION = 5.46

# https://vk.com/dev/permissions
SCOPES = ','.join([
    'friends', 'messages', 'offline',
])
APP_ID = 6127623

MAX_DIALOGS_COUNT_PER_TIME = 200
MAX_PHOTOS_COUNT_PER_TIME = 200
MAX_REQUEST_COUNT_PER_TIME = 25

RESULTS_DIR = 'results'
PRIVATE_DIALOGS_DIR = 'Private dialogs'
GROUP_DIALOGS_DIR = 'Group dialogs'
USER_INPUT_DIALOGS_DIR = 'User input dialogs'


def process_dialogs_photos(vk_api: 'vk.Api object', dialogs: list) -> list:
    """
    Подгружает фотки для указанных диалогов с помощью метода execute
    (https://vk.com/dev/execute)
    :param vk_api: объект vk.API, позволяющий работать с API vk
    :param dialogs: список словарей вида:
    {
        "name": Название диалога,
        "next_from": С какого элемента начать загрузку фоток,
        "id": Идентификатор диалога
    }
    :return: список словарей вида:
    {
        "processed": Логическое поле, указывающее, получилось ли подтянуть
            все фото для диалога (в методе execute можно выполнить макс 25 запросов)
        "name": Название диалога,
        "next_from": С какого элемента начать загрузку фоток при следующей итерации,
        "id": Идентификатор диалога,
        "photos": Список объектов фотографий (https://vk.com/dev/objects/photo API 5.65)
    }
    """
    vk_script_code = """
    var dialogs = {dialogs};
    var dialogsPhotos = [];
    var count_per_time = parseInt({max_photos_count_per_time});

    var i = 0;
    var request_count = 0;
    var dialogs_processed_count = 0;
    while ((request_count < {max_request_count_per_time}) &&
            (dialogs_processed_count < dialogs.length)) {{

        var processed = false;
        var photos = [];
        var next_from = dialogs[i].next_from;
        var peer_id = parseInt(dialogs[i].id);

        var photos_data = API.messages.getHistoryAttachments({{
            "peer_id": peer_id,
            "count": count_per_time,
            "start_from": next_from,
            "media_type": "photo",
            "v": {api_version},
        }});

        request_count = request_count + 1;
        next_from = photos_data.next_from;
        photos = photos + photos_data.items@.photo;

        while ((request_count < {max_request_count_per_time}) &&
               (photos_data.items.length >= parseInt({max_photos_count_per_time}))) {{

            photos_data = API.messages.getHistoryAttachments({{
                "peer_id": peer_id,
                "count": count_per_time,
                "start_from": next_from,
                "media_type": "photo",
                "v": {api_version},
            }});
            request_count = request_count + 1;
            next_from = photos_data.next_from;
            photos = photos + photos_data.items@.photo;
        }}

        if (photos_data.items.length < {max_photos_count_per_time}) {{
            processed = true;
            dialogs_processed_count = dialogs_processed_count + 1;
        }}

        dialogsPhotos.push({{
            "name": dialogs[i].name,
            "id": peer_id,
            "processed": processed,
            "photos": photos,
            "next_from": next_from,
        }});
        i = i + 1;
    }}
    return dialogsPhotos;
    """.format(
        dialogs=dialogs,
        max_photos_count_per_time=MAX_PHOTOS_COUNT_PER_TIME,
        max_request_count_per_time=MAX_REQUEST_COUNT_PER_TIME,
        api_version=VK_PHOTO_API_VERSION,
    ).replace('\'', '"')
    return vk_api.execute(code=vk_script_code)


def split_group_and_private_dialogs(vk_api: 'vk.Api object', dialogs: list) -> list:
    """
    Разделяет диалоги на личные и групповые

    Получает на вход список диалогов, полученных методом vk_api.messages.getDialogs,
    отделяет личные беседы от групповых
    Для каждого диалога подтягивает название: для групповых это название беседы,
    для личных - Имя Фамилия собеседника
    :param vk_api: объект vk.API, позволяющий работать с API vk
    :param dialogs: список объектов диалогов (https://vk.com/dev/messages.getDialogs)
    :return: кортеж вида:
    (
        # Список групповых диалогов
        [
            {
                'id': Идентификатор диалога,
                'name': Название диалога
            },
            ...
        ],
        # Список приватных диалогов
        [
            {
                'id': Идентификатор диалога,
                'name': Название диалога
            },
            ...
        ]
    )
    """
    group_dialogs = []
    private_dialogs = []
    for dialog in dialogs:
        if 'chat_id' in dialog['message']:
            group_dialogs.append({
                # id беседы = id беседы + 2000000000
                # https://vk.com/dev/messages.getHistoryAttachments
                'name': dialog['message']['title'],
                'id': dialog['message']['chat_id'] + 2000000000
            })
        elif dialog['message']['user_id'] > 0:
            # Исключаем чаты с ботами за стикеры и чаты, которые создаются
            # при получении подарков/стикеров
            # Исключаем ботов, оповещения МЧС и проч.
            private_dialogs.append({
                'name': '',
                'id': dialog['message']['user_id'],
            })

    # Получаем имена и фамилии пользователей, с которыми есть приватные беседы
    users_info = vk_api.users.get(
        user_ids=[dialog['id'] for dialog in private_dialogs]
    )
    for user, dialog in zip(users_info, private_dialogs):
        assert user['id'] == dialog['id'], "Smth went wrong, try again"
        dialog['name'] = '{0} {1}'.format(user['first_name'],
                                          user['last_name'])
    return group_dialogs, private_dialogs


def get_user_dialogs(vk_api: 'vk.Api object') -> list:
    """
    Получает список диалогов пользователя
    :param vk_api: объект vk.API, позволяющий работать с API vk
    :return: список объектов диалогов: https://vk.com/dev/messages.getDialogs
    """
    offset = 0
    dialogs = []
    while True:
        dialogs_data = vk_api.messages.getDialogs(
            count=MAX_DIALOGS_COUNT_PER_TIME, offset=offset
        )['items']
        offset += MAX_DIALOGS_COUNT_PER_TIME
        dialogs.extend(dialogs_data)

        # Если в запросе пришло меньше, чем было указано (макс возможное кол-во),
        # то значит, что мы получили данные о последних диалогах пользователя
        if len(dialogs_data) < MAX_DIALOGS_COUNT_PER_TIME:
            break
    return dialogs


def get_dialogs_photos(vk_api: 'vk.Api object', dialogs: list) -> list:
    """
    Подгружает фотки для указанных диалогов с помощью функции process_dialogs_photos

    Функцияя process_dialogs_photos может вернуть данные о обработанном диалоге
    с пометкой processed: False.
    Это означает, что функция не смогла подтянуть для данного диалога все фото-
    графии, т.к. исчерпан лимит запросов (в методе execute можно выполнить
    до 25 запросов)

    Данная функция итеративно вызывает process_dialogs для необработанных диалогов,
    пока для каждого диалога не будут подгружены все его фотографии

    :param vk_api: объект vk.API, позволяющий работать с API vk
    :param dialogs: список словарей вида:
    {
        "name": Название диалога,
        "id": Идентификатор диалога
    }
    :return: список словарей вида:
    {

        "name": Название диалога,
        "id": Идентификатор диалога,
        "photos": Список объектов фотографий (https://vk.com/dev/objects/photo API 5.65)
    }
    """
    not_processed_dialogs = dialogs
    previous_not_processed_dialog = None
    result_dialogs_photos = []
    while True:
        processed_dialogs = process_dialogs_photos(vk_api, not_processed_dialogs)
        # Если на прошлый итерациях остался необработанный диалог,
        # то смотрим, обработался ли он на этот раз
        if previous_not_processed_dialog:
            for dialog in processed_dialogs:
                # Добавляем новый фоточки, которые для него подтянули
                if dialog['id'] == previous_not_processed_dialog['id']:
                    previous_not_processed_dialog['photos'].extend(dialog['photos'])
                    # Если в этот раз он обработался, то заносим его в список
                    # результатов. И помечаем, что неообработанных диалогов нет
                    if dialog['processed']:
                        previous_not_processed_dialog['processed'] = True
                        result_dialogs_photos.append(previous_not_processed_dialog)
                        previous_not_processed_dialog = None
                        processed_dialogs.remove(dialog)
                    break

        current_not_processed_dialog = None
        for dialog in processed_dialogs:
            # В случае, если из-за ограничений метода execute (не более 25 запросов),
            # наш код не смог подгрузить все фотки для определенного диалога,
            # он вернет dialog['processed'] = False
            # Находим такой диалог
            if not dialog['processed']:
                current_not_processed_dialog = dialog
                if not previous_not_processed_dialog or \
                                current_not_processed_dialog['id'] != \
                                previous_not_processed_dialog['id']:
                    previous_not_processed_dialog = current_not_processed_dialog
                break

        _not_processed_dialogs = []
        if current_not_processed_dialog:
            _not_processed_dialogs.append({
                'name': current_not_processed_dialog['name'],
                'id': current_not_processed_dialog['id'],
                'next_from': current_not_processed_dialog['next_from']
            })
            processed_dialogs.remove(current_not_processed_dialog)

        # Если обработали все диалоги, то выходим из цикла
        result_dialogs_photos.extend(processed_dialogs)
        if not current_not_processed_dialog and \
                len(result_dialogs_photos) == len(dialogs):
            break

        processed_dialogs_ids = [dialog['id'] for dialog in
                                 result_dialogs_photos]
        for dialog in dialogs:
            # Не добавляем диалог, который находится в обработке
            # (использую continue, т.к. не хотел писать охуенно большой if-statement)
            if current_not_processed_dialog and dialog['id'] == \
                    current_not_processed_dialog['id']:
                continue

            if dialog['id'] not in processed_dialogs_ids:
                _not_processed_dialogs.append(dialog)

        not_processed_dialogs = _not_processed_dialogs

    # Удаляем служебные поля
    for dialog in result_dialogs_photos:
        dialog.pop('processed')
        dialog.pop('next_from')
    return result_dialogs_photos


def write_photos_urls_to_files(dir_name: str, dialogs_photos: list):
    """
    Выгружает URL фотографий в файлы

    Внутри каталога RESULTS_DIR Создает каталог с именем dir_name.
    Внутри каталога создает файлы dialog_name.txt,
    где dialog_name - имя диалога содержащий в себе URL фотографий,
    разделенных символом новой строки "\n"

    :param dir_name: имя каталога
    :param dialogs_photos: список словарей вида:
    {
        "name": Название диалога,
        "id": Идентификатор диалога,
        "photos": список URL фотографий
    }
    :return:
    """
    current_dir = os.path.dirname(os.path.abspath(__file__))
    results_dir = os.path.join(current_dir, RESULTS_DIR, dir_name)

    if not os.path.exists(results_dir):
        os.makedirs(results_dir)

    for dialog in dialogs_photos:
        file_name = '{0}.txt'.format(os.path.join(results_dir, dialog['name']))
        with open(file_name, 'w') as f:
            photos_urls = get_photos_urls(dialog['photos'])
            f.write('\n'.join(photos_urls))


def get_photos_urls(photos: list) -> list:
    """
    Переводит photo object в URL фотки в наилучшем качестве
    :param photos: Список объектов фотографий (https://vk.com/dev/objects/photo API 5.65)
    :return: Список URLs фотографий
    """
    photos_urls = []
    for photo in photos:
        # Вытаскиваем URL на фотку в наилучшем разрешении
        if 'photo_2560' in photo:
            photos_urls.append(photo['photo_2560'])
        elif 'photo_1280' in photo:
            photos_urls.append(photo['photo_1280'])
        elif 'photo_807' in photo:
            photos_urls.append(photo['photo_807'])
        elif 'photo_604' in photo:
            photos_urls.append(photo['photo_604'])
        elif 'photo_130' in photo:
            photos_urls.append(photo['photo_130'])
        elif 'photo_75' in photo:
            photos_urls.append(photo['photo_75'])
    return photos_urls


def create_parser() -> 'ArgumentParser object':
    """
    Создает объект ArgumentParser для последующего парсинга аргументов коммандной
    строки

    :return: объект ArgumentParser
    """
    parser = argparse.ArgumentParser(
        prog='vk_dialogs_photos_downloader',
        description="""
        Программа осуществляет выгрузку ссылок на фотографии в высоком разрешении
        из диалогов социальной сети "Вконтакте"
        """,
    )
    parser.add_argument(
        '-t', '--token', help='Access token',
    )
    parser.add_argument(
        '-u', '--user', help='Email или телефон пользователя'
    )
    parser.add_argument(
        '-p', '--password', help='Пароль пользователя'
    )
    parser.add_argument(
        '-a', '--app_id',
        help='ID приложения в VK.'
             ' По умолчанию используется приложение, созданное автором'
    )
    parser.add_argument(
        '-d', '--dialogs_ids', type=int, nargs='+',
        help='Список ID диалогов, для которых нужно выгрузить фотографии.'
             ' По умолчанию будут скачаны фотографии из всех диалогов'
    )
    return parser


def create_api_object(cmd_args) -> 'vk.API object':
    """
    Создает объект vk.API для последующей работы с API vk

    В зависимости от переданных аргументов командной строки производит автори-
    зацию  в вконтакте и возвращает объект, позволяющий работать с API соц. сети

    :param cmd_args: список аргументов командной строки полученный методом
    parse_args() объекта ArgumentParser
    :return: vk.API объект
    """
    if cmd_args.user and cmd_args.password:
        app_id = cmd_args.app_id if cmd_args.app_id else APP_ID
        session = vk.AuthSession(
            app_id=app_id, user_login=cmd_args.user,
            user_password=cmd_args.password, scope=SCOPES
        )
    elif cmd_args.token:
        session = vk.Session(access_token=cmd_args.token)
    else:
        raise ValueError('Необходимо указать --token или --user и --password')
    return vk.API(session, v=VK_API_VERSION)


def get_user_input_dialogs(vk_api: 'vk.API object', dialogs_ids: list) -> list:
    """
    По ID введенных пользователем диалогов, возвращает список объектов диалогов

    :param vk_api: vk.API объект
    :param dialogs_ids: список идентификаторов
    :return: список вида
    [
        {
            'id': идентификатор диалога (id + 2000000000 в случае групповой беседы)
            'name': название диалога (в данном случае - введенный пользовталем id),
        },
        ...
    ]
    """
    user_all_dialogs = get_user_dialogs(vk_api)
    user_input_dialogs = []

    for dialog_id in dialogs_ids:
        for dialog in user_all_dialogs:
            # Случай группового диалога
            if 'chat_id' in dialog['message'] and dialog['message']['chat_id'] == dialog_id:
                user_input_dialogs.append({
                    # id беседы = id беседы + 2000000000
                    'id': dialog_id + 2000000000,
                    # Пох на имена диалогов
                    'name': str(dialog_id)
                })
                break
            # Случай приватной диалога
            elif 'user_id' in dialog['message'] and dialog['message']['user_id'] == dialog_id:
                user_input_dialogs.append({
                    'id': dialog_id,
                    # Пох на имена диалогов
                    'name': str(dialog_id)
                })
                break

    return user_input_dialogs


def main():
    parser = create_parser()
    cmd_args = parser.parse_args()

    api = create_api_object(cmd_args)

    if cmd_args.dialogs_ids:
        input_dialogs = get_user_input_dialogs(api, cmd_args.dialogs_ids)
        if not input_dialogs:
            raise ValueError('У пользователя нет диалогов с указанными id')
        write_photos_urls_to_files(
            USER_INPUT_DIALOGS_DIR, get_dialogs_photos(api, input_dialogs)
        )
    else:
        user_dialogs = get_user_dialogs(api)
        group_dialogs, private_dialogs = split_group_and_private_dialogs(api, user_dialogs)

        private_dialogs_photos = get_dialogs_photos(api, private_dialogs)
        write_photos_urls_to_files(PRIVATE_DIALOGS_DIR, private_dialogs_photos)

        group_dialogs_photos = get_dialogs_photos(api, group_dialogs)
        write_photos_urls_to_files(GROUP_DIALOGS_DIR, group_dialogs_photos)


if __name__ == '__main__':
    main()
